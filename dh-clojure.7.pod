
=head1 NAME

dh-clojure - debhelper infrastructure for Clojure packages

=head1 DESCRIPTION

B<dh-clojure> is intended to help with the packaging of Clojure
projects.

See F</usr/share/doc/dh-clojure/example/rules> for a sample
debian/rules file, and L<dh-clojure-lein(7)>
for additional information.

=head1 FILES

=over

=item F</usr/share/doc/dh-clojure/example/rules>

=item F</usr/share/doc/debhelper/PROGRAMMING.gz>

=back

=head1 SEE ALSO

L<dh-clojure-lein(7)> and L<debhelper(7)>
