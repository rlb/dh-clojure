dh-clojure - debhelper extension to assist with Debian Clojure packaging
========================================================================

Currently only Leiningen based projects are supported.

The `example-clojure/` directory contains a trivial dh-clojure based
package that provides an example, is handy for testing during
development, and is also used to test dh-clojure itself via
`dh_auto_test`.

Much of the code can be exercised/diagnosed via
```
debian/rules clean
fakeroot debian/rules binary
```
This will also build the `example-clojure` package.

See [`dh-clojure-lein(7)`](./dh-clojure-lein.7.pod) for further
information.

dh-clojure internals
--------------------

Everything is driven by the `leiningen.pm` debhelper "buildsystem"
(see `/usr/share/doc/debhelper/PROGRAMMING.md.gz`).  It implements
subroutines for `dh_auto_*` packaging command sequences for building
Leiningen-based Clojure projects.

The Leiningen support depends on a Debian patch to C<lein> that causes
it to invoke Debian-specifc code at certain points during the build.
See [`dh-clojure-lein(7)`](./dh-clojure-lein.7.pod).

`debian/dh-clojure/` is reserved for dh-clojure's use.  If possible,
keep all of the paths that dh-clojure generates during the build in
`debian/dh-clojure/tmp` so that `dh_auto_clean` only has to remove
that directory, and for now, an empty `debian/dh-clojure` will always
be removed on clean.

Source
------

The `dh-clojure-lein` directory contains the `debian.dh-clojure-lein`
library that handles Debian-related project file adjustments.

Testing
-------

"All" the tests are run during the package build, but you can also run
the `dh-clojure-lein` tests directly:

    cd dh-clojure-lein && lein test
