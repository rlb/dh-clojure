(defproject example "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :licenses [{:name "EPL-2.0" :url "https://www.eclipse.org/legal/epl-2.0/"}
             {:name "LGPL-2.1+" :url "https://www.gnu.org/licenses/lgpl-2.1"}]
  :dependencies [[org.clojure/clojure "nope"]
                 [com.taoensso/truss "nope"]]
  :main ^:skip-aot example.core
  :target-path "target/%s"
  :classifiers [["test" :testutils]]
  :profiles {:testutils {:source-paths ^:replace ["test"]}
             :uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
