(ns example.core-test
  (:require [example.core :as core]
            [clojure.test :refer :all]))

(deftest test-example
  (testing "nothing"
    (is (nil? (core/nothing)))))
